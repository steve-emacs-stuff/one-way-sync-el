;;; -*- lexical-binding: t -*-
;; ows.el - One Way Syncing - sync local directories via rsync on buffer save
;;
;; Author: Stephen Meister
;; URL   : https://gitlab.com/pallagun
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GN  U General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; you're going to need rsync if you want to use this.

;; uses rsync and your directories must end in a /
;; (setq ows-sets nil)
;; (setq my-set (ows-make-set-spec "~/my/project/directory/"
;;                                 "root@10.0.0.238:/var/www/html_git/"))
;; (ows-add-set 'myproject-stevedev my-set)
;; (pushnew 'ows-after-save-hook after-save-hook)

(require 's)

(defvar ows-sets nil
  "All the one way sync sets you have.

Stored as an alist of plists.")

(defvar ows-verbose t
  "Should ows do a lot of messaging as it works")

(defvar ows-remember t
  "Should ows remember your past sync settings?")

(defsubst ows-make-set-spec (source dest)
  (list :source (expand-file-name source)
        :rsync dest))

(defun ows-add-set (name set)
  (when ows-verbose
    (message "Adding ows set %s" name))
  (setf (alist-get name ows-sets)
        set)
  (ows--save-sets))

(defun ows--save-sets ()
  "Save the ows sets if you are configured to do so."
  (when ows-remember
    (let ((current-sets (ows--get-saved-sets))
          (save-file (locate-user-emacs-file "ows-sets")))
      (with-temp-file save-file
        (insert (prin1-to-string (append ows-sets current-sets)))))))

(defun ows--get-saved-sets ()
  "Get the saved sets from teh same file."
  (let ((save-file (locate-user-emacs-file "ows-sets")))
    (with-temp-buffer
      (insert-file-contents-literally save-file)
      ;; TODO - isn't there some unique-by-predicate sequence function?
      (cl-loop with unique-specs = nil
               for spec in (car (read-from-string (buffer-string)))
               if (null (alist-get (car spec) unique-specs nil t 'string-equal))
               do (push spec unique-specs)
               finally return unique-specs))))

(defun ows--get-destination-matching-source (source-dir)
  "Determine the best guess for a destination given a source and saved sets"
  (let ((best-set (first
                   (seq-filter (lambda (set)
                                 (let ((set-data (cdr set)))
                                   (equal source-dir
                                          (plist-get set-data :source))))
                               (ows--get-saved-sets)))))
    (when best-set
      (plist-get (cdr best-set) :rsync))))

(defun ows--get-name-matching-source (source-dir)
  "Determine the best guess for a name given a source and saved sets"
  (let ((best-set (first
                   (seq-filter (lambda (set)
                                 (let ((set-data (cdr set)))
                                   (equal source-dir
                                          (plist-get set-data :source))))
                               (ows--get-saved-sets)))))
    (when best-set (car best-set))))
      
(defun ows--path-relative-to-set-source (file-name set-spec)
  "return the relative path of file-name to the source of set, or nil if it's not in the set."
  (let* ((set-source (plist-get set-spec :source))
         (set-source-length (length set-source)))
    (if (and (>= (length file-name) set-source-length)
             (equal (substring file-name 0 set-source-length)
                    set-source))
        (substring file-name set-source-length)
      nil)))

(defun ows--resolve-dest-file (relative-file-name set-spec)
  (format "%s%s" (plist-get set-spec :rsync) relative-file-name))
  
(defun ows--rsync-file (source-file dest-file)
  (when ows-verbose
    (message "ows start: %s" `("rsync" "-a" ,source-file ,dest-file)))
  (make-process :name "ows-sync"
                :command `("rsync" "-a" ,source-file ,dest-file)
                :sentinel 'ows--rsync-sentinel))

(defun ows--rsync-all-files (source dest)
  (when ows-verbose 
    (message "ows start: %s" `("rsync" "-a" ,source ,dest)))
  (make-process :name "ows-sync"
                :command `("rsync" "-a" ,source ,dest)
                :sentinel 'ows--rsync-sentinel))

(defun ows--ensure-after-save-hook ()
  "Make sure the ows-after-save-hook is added to the after-save-hook"
  (cl-pushnew 'ows-after-save-hook after-save-hook))

(defun ows--rsync-sentinel (process event)
  (if ows-verbose
      (message "ows %s: %s" event (process-command process))
    (message "ows: %s" (string-trim event))))

(defun ows--get-sets-for-file (file-name)
  "Return a listing of relative file name and ows-sets that file-name applies to.

Return is of the form:
((relative-file-name1 ows-set1) (relative-file-name2 ows) ...)"
  (seq-filter
   'identity
   (mapcar (lambda (ows-set)
             (let ((set-name (car ows-set))
                   (set-spec (cdr ows-set)))
               (let ((relative-file-name (ows--path-relative-to-set-source file-name set-spec)))
                 (if relative-file-name
                     (list relative-file-name ows-set)
                   nil))))
           ows-sets)))

(defun ows--sync-if-possible (file-name)
  "if the file-name'd file is in one of the sets, sync it."
  ;; todo - this should use ows--get-sets-for-file
  (cl-loop for ows-set-by-name in ows-sets
           for set-name = (car ows-set-by-name)
           for set-spec = (cdr ows-set-by-name)
           for relative-file-name = (ows--path-relative-to-set-source file-name set-spec)
           when relative-file-name
           do (ows--rsync-file file-name (ows--resolve-dest-file relative-file-name set-spec))))

(defun ows-full-sync ()
  "Full sync if possible on current file's set."
  (interactive)
  (let ((found-in-any-sets)
        (file-name (or (buffer-file-name)
                       (format "%s*" default-directory))))
    (cl-loop for ows-set-by-name in ows-sets
             for set-spec = (cdr ows-set-by-name)
             for relative-file-name = (ows--path-relative-to-set-source file-name set-spec)
             when relative-file-name
             do (progn
                  (setq found-in-any-sets t)
                  (ows--rsync-all-files (plist-get set-spec :source)
                                        (plist-get set-spec :rsync))))
    (unless found-in-any-sets
      (message "Path was not found in any ows sets: %s" file-name))))
  
(defun ows-after-save-hook ()
  (ows--sync-if-possible (buffer-file-name)))

(defun ows-info ()
  "Get info for any ows sets that the current buffer is in."
  (interactive)
  (let ((sets (ows--get-sets-for-file (or (buffer-file-name)
                                          default-directory))))
    (if sets
        (message "ows: %s"
                 (mapconcat (lambda (file-and-set)
                              (format "(%s \"%s\")"
                                      (car (second file-and-set))
                                      (first file-and-set)))
                            sets
                            ", "))
      (message "ows: file not in any sets"))))

(defun ows ()
  "One way sync entry function.

Get the project root and the destination.

TODO - make this one of those menu things, transient? I think?  Or a hydra?
"
  (interactive)
  (let* ((ensure-ending-slash (lambda (string)
                                (if (s-ends-with-p "/" string)
                                    string
                                  (format "%s/" string))))
         (default-source (if (symbol-function 'projectile-project-root)
                             (projectile-project-root)
                           default-directory))
         (source-dir (funcall ensure-ending-slash
                              (read-string "Source: " default-source)))
         (default-destination (ows--get-destination-matching-source source-dir))
         (destination-dir (read-string "Destination (rsync): " default-destination))
         (set-spec (ows-make-set-spec source-dir
                                      (funcall ensure-ending-slash destination-dir)))
         (default-name (ows--get-name-matching-source (funcall ensure-ending-slash destination-dir)))
         (set-name (read-string "Set name: " default-name)))
    (ows-add-set set-name set-spec)
    (ows--ensure-after-save-hook)))


(provide 'ows)
